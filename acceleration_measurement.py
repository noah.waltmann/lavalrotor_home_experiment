import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_blender.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#initializing data_dict
sensor_ID=sensor_settings_dict["ID"]
data_dict={sensor_ID:{"acceleration_x":[],"acceleration_y":[],"acceleration_z":[],"timestamp":[]}}

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#setting start and end time for measuring
t_end = time.time() + measure_duration_in_s
accelerometer.enable_motion_detection()
t_start = time.time()

# acceleration is measured and saved in data_dict
while time.time() < t_end:
    acc = accelerometer.acceleration
    timestamp = time.time()-t_start
    data_dict[sensor_ID]["acceleration_x"].append(acc[0])
    data_dict[sensor_ID]["acceleration_y"].append(acc[1])
    data_dict[sensor_ID]["acceleration_z"].append(acc[2])
    data_dict[sensor_ID]["timestamp"].append(timestamp)
    time.sleep(0.001)
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# group "RawData" is created in hdf5 file
with h5py.File(path_h5_file, 'w') as f:
    RawData = f.create_group("RawData")
    
    #datasets with data from data_dict are created
    acc_x = RawData.create_dataset('acceleration_x', data=data_dict[sensor_ID]["acceleration_x"])
    acc_y = RawData.create_dataset('acceleration_y', data=data_dict[sensor_ID]["acceleration_y"])
    acc_z = RawData.create_dataset('acceleration_z', data=data_dict[sensor_ID]["acceleration_z"])
    timest = RawData.create_dataset('timestamp', data=data_dict[sensor_ID]["timestamp"])
    
    #attributes are associated to datasets
    acc_x.attrs["unit"]="standard_gravity"
    acc_y.attrs["unit"]="standard_gravity"
    acc_z.attrs["unit"]="standard_gravity"
    timest.attrs["unit"]="seconds"
# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
