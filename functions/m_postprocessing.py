"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x:np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray: #: x:np.ndarray, y: np.ndarray, z: np.ndarray
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    result = np.sqrt(x*x+y*y+z*z)
    return result

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    
    # for every datapoint that is not the first of the measurement a value is interpolated 
    # time gaps between datapoints are the same everywhere
    
    new_time = np.arange(0,20,20/len(time))
    new_data=np.zeros(len(time))
    
    
    for i in range(len(time)):
        if i == 0:
            new_data[i]=data[i]
        else:
            new_data[i]=data[i-1]+(new_time[i]-time[i-1])*(data[i]-data[i-1])/(time[i]-time[i-1])
            
    return(new_data,new_time)

def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    #abtastfrequenz wird ermittelt 
    abt_freq = 1 / np.mean(np.diff(time)) 
    
    #furrietransformation 
    fft_ergebnis = np.fft.fft(x)
    
    #frequenzen 
    frequencies = np.fft.fftfreq(len(x), d=1/abt_freq)
    
    #negative werte werden ignoriert
    positive_hälfte = fft_ergebnis[:len(fft_ergebnis)//2]
    positive_frequenz = frequencies[:len(frequencies)//2]
    
    amplituden_spectrum = np.abs(positive_hälfte)
    
    
    return(amplituden_spectrum, positive_frequenz)